FROM ubuntu:22.04

LABEL maintainer="cschu1981@gmail.com"
LABEL version="0.4.7"
LABEL description="This is a Docker Image for an alignment workflow composed of bwa and samtools"


ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install -y wget python3-pip git dirmngr gnupg ca-certificates build-essential libssl-dev libcurl4-gnutls-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev bwa samtools
RUN apt clean
